﻿namespace Teste_16333
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {
        public void HideAstrsks()
        {
            labelAsteriskInf.Hide();
            labelAsteriskInf1.Hide();
            labelAsteriskInf2.Hide();
            labelAsteriskSal.Hide();
            labelAsteriskTit.Hide();
            labelAsteriskId.Hide();
            labelAsteriskTax.Hide();
            labelAsteriskCmp.Hide();
            labelAsteriskLvn.Hide();
            labelAsteriskMud.Hide();
        }

        //CartaoCliente cartao1;
        CartaoCliente[] cartao = new CartaoCliente[10];   //10 cartões máx, podem ser mil...

        int cartaoCorrente = 0;
        int nrCard;

        int frame = 1;

        public string cardInfo()
        {
            return labelInfo.Text = String.Format("----------- Dados do Cartão {0} -----------\n\n", cartaoCorrente + 1)
                + cartao[cartaoCorrente].ToString();
        }

        double valorCompra;
        string itemCompra;
        
        public Form1()
        {
            InitializeComponent();

            HideAstrsks();

            List<string> itemMenu = new List<string>()
            {
            "Lingerie:             50€", "S&M pack:             70€", "Sweatshirt Slayer:    95€",
            "Ténis da Mike:        20€", "Artigo esgotado:      39€", "Espigões com blusão: 380€",
            "Relógio Hublot:     7200€", "Croissant gourmet:    17€", "Shampoo 1-em-2:       25€",
            "Coke Dezeroed Decaf:   1€", "Fraldas Senior:      210€", "CD-Colectânea Flops:  10€",
            "Maria + Time Magazine:10€", "Barba + cabelo:     9.99€", "Contabilidade:     49.99€"
            };
        
            List<string> premioMenu = new List<string>()
            {                                                          
            "prém_Peluche:         30Pts", "prém_Bicicleta:       70Pts", "prém_Motorizada:     250Pts",
            "prém_Viagem:        2000Pts", "prém_Carro:         1000Pts", "prém_Mansarda:     10000Pts"
            };

            foreach (string item in itemMenu)
                comboBoxItems.Items.Add(item);

            foreach (string premio in premioMenu)
                comboBoxPremios.Items.Add(premio);

 

        }


        private void Form1_Load(object sender, EventArgs e)
        {
            if (cartao[cartaoCorrente] == null)
            {
                Pablo.Tick += Pablo_Tick;
                Pablo.Start();

                butTransf.Enabled = false;
                butAltTit.Enabled = false;
                butAltTax.Enabled = false;
                butComprar.Enabled = false;
                butLevantar.Enabled = false;
            }
        }

        private void butComprar_Click(object sender, EventArgs e)
        {
            HideAstrsks();

            if (comboBoxItems.SelectedItem == null)
            {
                labelAsteriskCmp.Show();
                labelAsteriskInf1.Show();
                labelAsteriskInf1.Text = "* Por favor selecione um arigo";
                return;
            }

            valorCompra = Convert.ToDouble(comboBoxItems.SelectedItem.ToString().Split(':')[1].Trim().TrimEnd('€'));
            itemCompra = comboBoxItems.SelectedItem.ToString().Split(':')[0];

            if (valorCompra > cartao[cartaoCorrente].Saldo)
            {
                labelAsteriskCmp.Show();
                labelAsteriskInf1.Show();
                labelAsteriskInf1.Text = "* Não tem saldo suficiente para efetuar a compra";
                return;
            }

            cartao[cartaoCorrente].EfetuarCompra(valorCompra, itemCompra);
            cardInfo();
            

            //System.IO.File.WriteAllText(@"D:\WriteText.txt", cartao[cartaoCorrente].ToString());

            //label6.Text = cartao[cartaoCorrente].PontosBonus.ToString();

            label6.Text = cartao[cartaoCorrente].factor_.ToString();
        }

        private void butCriar_Click(object sender, EventArgs e)
        {
            HideAstrsks();
            Regex alfaSpaces = new Regex("^[a-zA-Z' ']*$");

            if (!txtBoxID.Text.All(char.IsLetterOrDigit) || txtBoxID.Text == "")
            {
                labelAsteriskId.Show();
                labelAsteriskInf.Show();
                labelAsteriskInf.Text = "* Campo de Preenchimento obrigatório.\nRecebe apenas letras e/ou números.";
                return;
            }

            for (int i = 0; i < cartao.Length; i++)
                if (!(cartao[i] == null) && txtBoxID.Text == cartao[i].Id)
                {
                    labelAsteriskId.Show();
                    labelAsteriskInf.Show();
                    labelAsteriskInf.Text = "* Atenção! Número de cartão existente.";
                    return;
                }


            if (!alfaSpaces.IsMatch(txtBoxTit.Text) || txtBoxTit.Text == "" || txtBoxTit.Text.All(char.IsWhiteSpace))
            {
                labelAsteriskTit.Show();
                labelAsteriskInf.Show();
                labelAsteriskInf.Text = "* Campo de Preenchimento obrigatório.\nRecebe apenas letras";
                return;
            }

            if (!txtBoxSal.Text.All(char.IsDigit) || txtBoxSal.Text == "")
            {
                labelAsteriskSal.Show();
                labelAsteriskInf.Show();
                labelAsteriskInf.Text = "* Campo de Preenchimento obrigatório.\nRecebe apenas números";
                return;
            }

            nrCard = Convert.ToInt32(nrUpDown.Value) - 1;

            cartao[nrCard] = new CartaoCliente(txtBoxID.Text, txtBoxTit.Text, Convert.ToDouble(txtBoxSal.Text));

            cartaoCorrente = nrCard;
            cardInfo();

            txtBoxTax.Text = "10";

            butTransf.Enabled = true;
            butAltTit.Enabled = true;
            butAltTax.Enabled = true;
            butComprar.Enabled = true;
            butLevantar.Enabled = true;

            Pablo.Stop();
        }

        private void butLevantar_Click(object sender, EventArgs e)
        {
            HideAstrsks();

            if (comboBoxPremios.SelectedItem == null)
            {
                labelAsteriskLvn.Show();
                labelAsteriskInf2.Show();
                labelAsteriskInf2.Text = "* Por favor selecione um prémio";
                return;
            }

            string premioPremio = comboBoxPremios.SelectedItem.ToString().Split(':')[0];
            string premioPontos = comboBoxPremios.SelectedItem.ToString().Split(':')[1].Trim().Replace("Pts", String.Empty);

            if (Convert.ToInt32(premioPontos) > cartao[cartaoCorrente].Pontos)
            {
                labelAsteriskLvn.Show();
                labelAsteriskInf2.Show();
                labelAsteriskInf2.Text = "* Não tem pontos suficientes para levantar o prémio";
                return;
            }

            cartao[cartaoCorrente].LevantarPremio(Convert.ToInt32(premioPontos), premioPremio);
            cardInfo();
        }

        private void butAltTax_Click(object sender, EventArgs e)
        {
            if (txtBoxTax.Text == "")
                return;
            cartao[cartaoCorrente].AlterarTaxa(Convert.ToDouble(txtBoxTax.Text));
            cardInfo();
        }

        private void butAltTit_Click(object sender, EventArgs e)
        {
            HideAstrsks();
            Regex alfaSpaces = new Regex("^[a-zA-Z' ']*$");
            if (!alfaSpaces.IsMatch(txtBoxTit.Text) || txtBoxTit.Text == "" || txtBoxTit.Text.All(char.IsWhiteSpace))
            {
                labelAsteriskTit.Show();
                labelAsteriskInf.Show();
                labelAsteriskInf.Text = "* Campo de Preenchimento obrigatório.\nRecebe apenas letras";
                return;
            }
            cartao[cartaoCorrente].AlterarTitular(txtBoxTit.Text);
            cardInfo();

        }

        //private void txtBoxTit_GotFocus(object sender, EventArgs e)
        //{
            //labelInfo.Text = "-------------Dados do Cartão-------------\n\n\n\n" +
            //    "     /-----------------------------\\ \n" +
            //    "    (Insira o NOME. O meu é Pablito!)\n" +
            //    "     \\--------------\\/-------------/\n" +
            //    "          ______________|________\n" +
            //    "         /                       \\\n" +
            //    "        |     \"\"            \"\"    |\n" +
            //    "        |     __           __     |\n" +
            //    "        |    (0_)   ^ ^   (0_)    |\n" +
            //    "        (  )===================(  )\n" +
            //    "        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
            //    "        |  |                   |  | \n" +
            //    "        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
            //    "        |  \\===================/  |   \n" +

            //    "         \\==========(_|_)========/\n" +
            //    "                  ||  ||" +
            //    "                  ||  ||" +
            //    "                __||  ||__\n" +
            //    "              -===/===-\n" +
            //    "            -====/====-\n" +
            //    "        -=====/======-\n\n\n\n";
        //}

        private void butNrcard_Click(object sender, EventArgs e)
        {
            HideAstrsks();
            cartaoCorrente = Convert.ToInt32(nrUpDown.Value) - 1;
            if (cartao[cartaoCorrente] == null)
            {
                labelAsteriskMud.Show();
                labelAsteriskInf.Show();
                labelAsteriskInf.Text = String.Format("* Não existe cartão nr. {0}", cartaoCorrente + 1);
                return;
            }

            txtBoxID.Text = cartao[cartaoCorrente].Id;
            txtBoxTit.Text = cartao[cartaoCorrente].Titular;
            txtBoxSal.Text = cartao[cartaoCorrente].Saldo.ToString();
            txtBoxTax.Text = cartao[cartaoCorrente].Taxa.ToString();
            cardInfo();
        }

        private void butTransf_Click(object sender, EventArgs e)
        {
            if (txtBoxPnt.Text == "")
                return;

            cartao[cartaoCorrente].TranferirPontos(Convert.ToInt32(txtBoxPnt.Text), cartao[Convert.ToInt32(nrUpDown1.Value) - 1]);
            cardInfo();
            //nrUpDown1.Value = 3;
        }

        private void Pablo_Tick(object sender, EventArgs e)
        {
            label6.Text = txtBoxTit.Focused.ToString();
            if (!txtBoxTit.Focused)
                IntroAnim();
            else
                AnimName();
        }

        public void IntroAnim()
        {
            if (frame == 1)
            {
                frame++;
                labelInfo.Text = "-------------Dados do Cartão-------------\n\n\n\n" +
                    "     /-----------------------------\\\n" +
                    "    (                               )\n" +
                    "     \\-------------------\\/--------/\n" +
                    "                                 \n" +
                    "          _____^___________^_____\n" +
                    "         /     ||          ||    \\\n" +

                    "        |                         |\n" +
                    "        |                         |\n" +
                    "        |    (__)   ^ ^   (__)    |\n" +
                    "        (  )===================(  )\n" +
                    "        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "        |  |###################|  |\n" +
                    "        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "        |  \\===================/  |\n" +

                    "         \\==========(_|_)========/\n" +
                    "   ________________||__||_______________\n" +
                    //"                 ||  ||\n" +
                    "                   ||  ||\n" +
                    "                 __||  ||__\n" +
                    "                -====/====-\n" +
                    "              -====/====-\n" +
                    "           -=====/======-\n\n\n\n";
                return;
            }
            if (frame == 2)
            {
                frame++;
                labelInfo.Text = "-------------Dados do Cartão-------------\n\n\n\n" +
                    "     /-----------------------------\\\n" +
                    "    (  Ainda                        )\n" +
                    "     \\-------------------\\/--------/\n" +
                    "                                 \n" +
                    "          _______________________\n" +
                    "         /                       \\\n" +
                    "        |     \"\"            \"\"    |\n" +
                    "        |     __           __     |\n" +
                    "        |    (0_)   ^ ^   (0_)    |\n" +
                    "        (  )===================(  )\n" +
                    "        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    //"        |  |###################|  |\n" +
                    "        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "        |  \\===================/  |\n" +

                    "         \\==========(_|_)========/\n" +
                    "                 ||    ||\n" +
                    "  ___ ___________||____||______________\n" +
                    "                 ||    ||\n" +
                    "               __||    ||__\n" +
                    "              -=====/====-\n" +
                    "            -=====/====-\n" +
                    "         -======/======-\n\n\n\n";
                return;
            }
            if (frame == 3)
            {
                frame++;
                labelInfo.Text = "-------------Dados do Cartão-------------\n\n\n\n" +
                    "     /-----------------------------\\\n" +
                    "    (  Ainda não                    )\n" +
                    "     \\-------------------\\/--------/\n" +
                    "                                 \n" +
                    "          _______________________\n" +
                    "         /                       \\\n" +
                    "        |     \"\"            \"\"    |\n" +
                    "        |     __           __     |\n" +
                    "        |    (0_)   ^ ^   (0_)    |\n" +
                    "        (  )===================(  )\n" +
                    "        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "        |  |###################|  |\n" +
                    "        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "        |  \\===================/  |\n" +

                    "         \\==========(_|_)========/\n" +
                    //"             ||  || \n" +
                    "  ___________||__||____________________\n" +
                    "            ||   ||\n" +
                    "           /     ||__\n" +
                    "               /====-\n" +
                    "        -====/====-\n" +
                    "     -=====/======-\n\n\n\n";
                return;
            }
            if (frame == 4)
            {
                frame++;
                labelInfo.Text = "-------------Dados do Cartão-------------\n\n\n\n" +
                    " /-----------------------------\\\n" +
                    "(  Ainda não foi                )\n" +
                    " \\-------------------\\/--------/\n" +
                    "                                 \n" +
                    "      _______________________\n" +
                    "     /                       \\\n" +
                    "    |         \"\"            \"\"|\n" +
                    "    |     __           __     |\n" +
                    "    |    (0_)   ^ ^   (0_)    |\n" +
                    "    |(   )==================(   )\n" +
                    "    |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    //"    |  |###################|  |\n" +
                    "    |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "    |  \\===================/  |\n" +

                    "     \\==============(_|_)====/\n" +
                    "             ||  || \n" +
                    "  ___________||__||___________________\n" +
                    "             ||   ||\n" +
                    "           __||     \\\n" +
                    "          -====/     \n" +
                    "        -====/====-\n" +
                    "     -=====/======-\n\n\n\n";
                return;
            }
            if (frame == 5)
            {
                frame++;
                labelInfo.Text = "-------------Dados do Cartão-------------\n\n\n\n" +
                    " /-----------------------------\\\n" +
                    "(  Ainda não foi criado         )\n" +
                    " \\-------------------\\/--------/\n" +
                    "                                 \n" +
                    "      _______________________\n" +
                    "     /                       \\\n" +
                    "    |         \"\"            \"\"|\n" +
                    "    |     __           __     |\n" +
                    "    |    (0_)   ^ ^   (0_)    |\n" +
                    "    (  )===================(  )\n" +
                    "    |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "    |  |###################|  |\n" +
                    "    |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "    |  \\===================/  |\n" +

                    "     \\==============(_|_)====/\n" +
                    //"         ||  || \n" +
                    "  ________||__||_______________________\n" +
                    "        ||   ||\n" +
                    "       /     ||__\n" +
                    "           /====-\n" +
                    "    -====/====-\n" +
                    " -=====/======-\n\n\n\n";
                return;
            }
            if (frame == 6)
            {
                frame++;
                labelInfo.Text = "-------------Dados do Cartão-------------\n\n\n\n" +
                    " /-----------------------------\\\n" +
                    "(  Ainda não foi criado CARTÃO! )\n" +
                    " \\---------------\\/------------/\n" +
                    "                                 \n" +
                    "  _______________________\n" +
                    " /                       \\\n" +
                    "|     \"\"            \"\"    |\n" +
                    "|     __           __     |\n" +
                    "|    (0_)   ^ ^   (0_)    |\n" +
                    "(  )===================(  )\n" +
                    "|  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    //"|  |###################|  |\n" +
                    "|  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "|  \\===================/  |\n" +

                    " \\==========(_|_)========/\n" +
                    "         ||  || \n" +
                    "   ______||__||__________________________\n" +
                    "         ||  ||\n" +
                    "       __||  ||__\n" +
                    "      -====/====-\n" +
                    "    -====/====-\n" +
                    " -=====/======-\n\n\n\n";
                return;
            }
            if (frame == 7)
            {
                frame++;
                labelInfo.Text = "-------------Dados do Cartão-------------\n\n\n\n" +
                    " /-----------------------------\\\n" +
                    "(  Ainda não foi criado CARTÃO! )\n" +
                    " \\---------------\\/------------/\n" +
                    "                                 \n" +
                    "  _______________________\n" +
                    " /                       \\\n" +
                    "|   \"\"        \"\"          |\n" +
                    "|   __           __       |\n" +
                    "|  (0_)   ^ ^   (0_)      |\n" +
                    "(  )===================(  )\n" +
                    "|  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "|  |###################|  |\n" +
                    "|  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "|  \\===================/  |\n" +

                    " \\==========(_|_)========/\n" +
                    //"         ||  || \n" +
                    "    _____||__||__________________________\n" +
                    "         ||  ||\n" +
                    "       __||  ||__\n" +
                    "      -====/====-\n" +
                    "    -====/====-\n" +
                    " -=====/======-\n\n\n\n";
                return;
            }
            if (frame == 8)
            {
                frame++;
                labelInfo.Text = "-------------Dados do Cartão-------------\n\n\n\n" +
                    " /-----------------------------\\\n" +
                    "(  Ainda não foi criado CARTÃO! )\n" +
                    " \\---------------\\/------------/\n" +
                    "                                 \n" +
                    "  _______________________\n" +
                    " /                       \\\n" +
                    "|     \"\"            \"\"    |\n" +
                    "|   __           __       |\n" +
                    "|  (0_)   ^ ^   (0_)      |\n" +
                    "(  )===================(  )\n" +
                    "|  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    //"|  |###################|  |\n" +
                    "|  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "|  \\===================/  |\n" +

                    " \\============/ \\========/\n" +
                    "         ||  (_|_)\n" +
                    "  _______||__||__________ _____________\n" +
                    "         ||  ||\n" +
                    "       __||  ||__\n" +
                    "      -====/====-\n" +
                    "    -====/====-\n" +
                    " -=====/======-\n\n\n\n";
                return;
            }
            if (frame == 9)
            {
                frame++;
                labelInfo.Text = "-------------Dados do Cartão-------------\n\n\n\n" +
                    " /-----------------------------\\\n" +
                    "(  Ainda não foi criado cartão! )\n" +
                    " \\-------------------\\/--------/\n" +
                    "                                 \n" +
                    "      _______________________\n" +
                    "     /                       \\\n" +
                    "    | \"\"        \"\"            |\n" +
                    "    |                         |\n" +
                    "   | (__)   ^ ^   (__)        |\n" +
                    "   (   )==================(  )|\n" +
                    "    |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "    |  |###################|  |\n" +
                    "    |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "    |  \\=====(_|_)========/  |\n" +

                    "     \\=======================/\n" +
                    //"         ||  || \n" +
                    "  _______||__||________________________\n" +
                    "         ||   ||\n" +
                    "       __||     \\\n" +
                    "      -====/     \n" +
                    "    -====/====-\n" +
                    " -=====/======-\n\n\n\n";
                return;
            }
            if (frame == 10)
            {
                frame++;
                labelInfo.Text = "-------------Dados do Cartão-------------\n\n\n\n" +
                    " /-----------------------------\\\n" +
                    "(  Ainda não foi criado CARTÃO! )\n" +
                    " \\-------------------\\/--------/\n" +
                    "                                 \n" +
                    "      _______________________\n" +
                    "     /                       \\\n" +
                    "    |         \"\"            \"\"|\n" +
                    "    |     __           __     |\n" +
                    "    |    (_0)   ^ ^   (_0)    |\n" +
                    "    (  )===================(  )\n" +
                    "    |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    //"    |  |###################|  |\n" +
                    "    |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "    |  \\=========(_|_)=====/  |\n" +

                    "     \\=======================/\n" +
                    "             ||  || \n" +
                    " ____________||__||___________________\n" +
                    "            ||   ||\n" +
                    "           /     ||__\n" +
                    "               /====-\n" +
                    "        -====/====-\n" +
                    "     -=====/======-\n\n\n\n";
                return;
            }
            if (frame == 11)
            {
                frame++;
                labelInfo.Text = "-------------Dados do Cartão-------------\n\n\n\n" +
                    "     /-----------------------------\\\n" +
                    "    (  Ainda não foi criado cartão! )\n" +
                    "     \\-------------------\\/--------/\n" +
                    "                                 \n" +
                    "          _______________________\n" +
                    "         /                       \\\n" +
                    "        |     \"\"            \"\"    |\n" +
                    "        |     __           __     |\n" +
                    "        |    (_0)   ^ ^   (_0)    |\n" +
                    "        (  )===================(  )\n" +
                    "        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    //"        |  |###################|  |\n" +
                    //"        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "        |  \\===================/  |\n" +

                    "         \\==========(_|_)========/\n" +
                    "             ||  || \n" +
                    "             ||  || \n" +
                    " ____________||__||___________________\n" +
                    "             ||   ||\n" +
                    "           __||     \\\n" +
                    "          -====/     \n" +
                    "        -====/====-\n" +
                    "     -=====/======-\n\n\n\n";
                return;
            }
            if (frame == 12)
            {
                frame = 1;
                labelInfo.Text = "-------------Dados do Cartão-------------\n\n\n\n" +
                    "     /-----------------------------\\\n" +
                    "    (  Ainda não foi criado CARTÃO! )\n" +
                    "     \\-------------------\\/--------/\n" +
                    "                                 \n" +
                    "          _______________________\n" +
                    "         /                       \\\n" +
                    "        |     \"\"            \"\"    |\n" +
                    "        |     __           __     |\n" +
                    "        |    (_0)   ^ ^   (_0)    |\n" +
                    "        (  )===================(  )\n" +
                    "        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    //"        |  |###################|  |\n" +
                    //"        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "        |  \\===================/  |\n" +

                    "         \\==========(_|_)========/\n" +
                    "                   ||  ||\n" +
                    "                   ||  ||\n" +
                    "  _________________||__||______________\n" +
                    "                   ||  ||\n" +
                    "                 __||  ||__\n" +
                    "                -====/====-\n" +
                    "              -====/====-\n" +
                    "           -=====/======-\n\n\n\n";
                return;
            }
        }



        public void AnimName()
        {
            if (frame > 5) frame = 1;
            if (frame == 1)
            {
                frame++;
                labelInfo.Text = "-------------Dados do Cartão-------------\n\n\n\n" +
                    "     /-----------------------------\\\n" +
                    "    (                               )\n" +
                    "     \\-------------------\\/--------/\n" +
                    "                                 \n" +
                    "          _______________________\n" +
                    "         /                       \\\n" +
                    "        |     \"\"            \"\"    |\n" +
                    "        |     __           __     |\n" +
                    "        |    (0_)   ^ ^   (0_)    |\n" +
                    "        (  )===================(  )\n" +
                    "        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    //"        |  |                   |  |\n" +
                    "        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "        |  \\===================/  |\n" +

                    "         \\==========(_|_)========/\n" +
                    "_________________||__||________________\n" +
                    "                 ||  ||              \n" +
                    "               __||  ||__\n\n" +
                    "\n\n" +
                    "            -====/====-\n" +
                    "         -=====/======-\n\n\n\n";
                return;
            }

            if (frame == 2)
            {
                frame++;
                labelInfo.Text = "-------------Dados do Cartão-------------\n\n\n\n" +
                    "       /-----------------------------\\\n" +
                    "      (Insira o                       )\n" +
                    "       \\-------------------\\/--------/\n" +

                    "                                 \n\n\n" +
                    "          _______________________\n" +
                    "         /                       \\\n" +
                    "        |     \"\"            \"\"    |\n" +
                    "        |     __           __     |\n" +
                    "        |    (_0)   ^ ^   (_0)    |\n" +
                    "        (  )===================(  )\n" +
                    "        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "        |  |                   |  |\n" +
                    " _______|  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |____\n" +
                    "        |  \\===================/  |\n" +

                    "        \\============/ \\=========/\n" +
                    "                 || (_|_)            \n" +
                    //"                 ||  ||\n" +
                    "               __||  ||__\n" +
                    "              -===/===-\n" +
                    "           -====/====-\n" +
                    "        -=====/======-\n\n\n\n";
                return;
            }

            if (frame == 3)
            {
                frame++;
                labelInfo.Text = "-------------Dados do Cartão-------------\n\n\n\n" +
                    "     /-----------------------------\\\n" +
                    "    (Insira o NOME. O meu           )\n" +
                    "     \\-------------------\\/--------/\n" +
                    //"          ______________|________\n" +
                    "         /   \"\"            \"\"    \\\n" +
                    "        |     __           __     |\n" +
                    "        |    (0)    ^ ^    (0)    |\n" +
                    "        (  )===================(  )\n" +
                    "        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "        |  |                   |  |\n" +
                    "        |  |                   |  |\n" +
                    "        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "        |  \\========(_|_)======/  |\n" +

                    "         \\=======================/\n" +
                    "                  ||  ||\n" +
                    "________________  ||__||______________\n" +
                    "                  ||  ||\n" +
                    "                  ||  ||\n" +
                    "                __||  ||__\n" +
                    "\n\n               -===/===-\n" +
                    "            -====/====-\n" +
                    "         -=====/======-\n\n\n\n";
                return;
            }

            if (frame == 4)
            {
                frame++;
                labelInfo.Text = "-------------Dados do Cartão-------------\n\n\n\n" +
                    "    /-----------------------------\\\n" +
                    "   (Insira o NOME. O meu é PABLITO!)\n" +
                    "    \\-------------------\\/--------/\n" +
                    //"          ______________|________\n" +
                    //"         /   \"\"            \"\"    \\\n" +
                    "      *´|   (X)     ^ ^     (X)   |*\n" + "        |     __           __     |\n" +

                    "       (    )=================(    )\n" +
                    "        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "        |  |                   |  |\n" +
                    "        |  |/\\/\\/\\/\\(_|_)\\/\\/\\/|  |\n" +
                    "        |  \\===================/  |\n" +


                    "         \\=======================/\n" +
                    "                  ||  ||\n" +
                    "                  ||  ||\n" +
                    "                  ||  ||\n" +
                    "__________________||__||__________\n" +
                    "                  ||  ||\n" +
                    "                __||  ||__\n" +
                    "\n               -===/===-\n" +
                    "            -====/====-\n" +
                    "         -=====/======-\n\n\n\n";
                return;
            }


            if (frame == 5)
            {
                frame = 1;
                labelInfo.Text = "-------------Dados do Cartão-------------\n\n\n\n" +

                    "    /-----------------------------\\\n" +
                    "   (Insira o NOME. O meu é PABLITO!)\n" +
                    "    \\-------------------\\/--------/\n" +
                    //"          ______________|________\n" +
                    //"         /   \"\"            \"\"    \\\n" +
                    " ´      |   (X)     ^ ^     (X)   |  *\n" + 
                    "    *   |     __           __     | `\n" +

                    "       (    )=======(_|_)=====(    )\n" +
                    "        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "        |  |                   |  |\n" +
                    "        |  |/\\/\\/\\/\\/\\/\\/\\/\\/\\/|  |\n" +
                    "        |  \\===================/  |\n" +


                    "         \\=======================/\n" +
                    "                  ||  ||\n" +
                    "                  ||  ||\n" +
                    "                  ||  ||\n" +
                    "__________________||__||_______________\n" +
                    "                __||  ||__\n" +
                    "\n\n               -===/===-\n" +
                    "            -====/====-\n" +
                    "         -=====/======-\n\n\n\n";
                return;
            }
        }

    }
}
