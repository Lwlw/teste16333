﻿namespace Teste_16333
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.comboBoxItems = new System.Windows.Forms.ComboBox();
            this.labelInfo = new System.Windows.Forms.Label();
            this.txtBoxTit = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBoxSal = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBoxTax = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtBoxID = new System.Windows.Forms.TextBox();
            this.butComprar = new System.Windows.Forms.Button();
            this.butCriar = new System.Windows.Forms.Button();
            this.butAltTax = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelAsteriskMud = new System.Windows.Forms.Label();
            this.nrUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.butNrcard = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.nrUpDown = new System.Windows.Forms.NumericUpDown();
            this.labelAsteriskTax = new System.Windows.Forms.Label();
            this.txtBoxPnt = new System.Windows.Forms.TextBox();
            this.labelAsteriskSal = new System.Windows.Forms.Label();
            this.labelAsteriskTit = new System.Windows.Forms.Label();
            this.butTransf = new System.Windows.Forms.Button();
            this.labelAsteriskId = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.butAltTit = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelAsteriskCmp = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.labelAsteriskLvn = new System.Windows.Forms.Label();
            this.comboBoxPremios = new System.Windows.Forms.ComboBox();
            this.butLevantar = new System.Windows.Forms.Button();
            this.labelAsteriskInf = new System.Windows.Forms.Label();
            this.labelAsteriskInf2 = new System.Windows.Forms.Label();
            this.labelAsteriskInf1 = new System.Windows.Forms.Label();
            this.Pablo = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nrUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nrUpDown)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxItems
            // 
            this.comboBoxItems.BackColor = System.Drawing.Color.White;
            this.comboBoxItems.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxItems.Font = new System.Drawing.Font("Consolas", 7.75F, System.Drawing.FontStyle.Italic);
            this.comboBoxItems.FormattingEnabled = true;
            this.comboBoxItems.Location = new System.Drawing.Point(85, 21);
            this.comboBoxItems.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.comboBoxItems.Name = "comboBoxItems";
            this.comboBoxItems.Size = new System.Drawing.Size(149, 20);
            this.comboBoxItems.TabIndex = 0;
            // 
            // labelInfo
            // 
            this.labelInfo.BackColor = System.Drawing.Color.White;
            this.labelInfo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelInfo.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfo.Location = new System.Drawing.Point(285, 12);
            this.labelInfo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(255, 382);
            this.labelInfo.TabIndex = 2;
            // 
            // txtBoxTit
            // 
            this.txtBoxTit.BackColor = System.Drawing.Color.White;
            this.txtBoxTit.Location = new System.Drawing.Point(61, 49);
            this.txtBoxTit.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtBoxTit.Name = "txtBoxTit";
            this.txtBoxTit.Size = new System.Drawing.Size(111, 20);
            this.txtBoxTit.TabIndex = 2;
            this.txtBoxTit.Text = "Dane Joe";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 52);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Titular:";
            // 
            // txtBoxSal
            // 
            this.txtBoxSal.BackColor = System.Drawing.Color.White;
            this.txtBoxSal.Location = new System.Drawing.Point(61, 75);
            this.txtBoxSal.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtBoxSal.Name = "txtBoxSal";
            this.txtBoxSal.Size = new System.Drawing.Size(111, 20);
            this.txtBoxSal.TabIndex = 3;
            this.txtBoxSal.Text = "100000";
            this.txtBoxSal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 79);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Saldo:";
            // 
            // txtBoxTax
            // 
            this.txtBoxTax.BackColor = System.Drawing.Color.White;
            this.txtBoxTax.Location = new System.Drawing.Point(111, 161);
            this.txtBoxTax.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtBoxTax.Name = "txtBoxTax";
            this.txtBoxTax.Size = new System.Drawing.Size(48, 20);
            this.txtBoxTax.TabIndex = 5;
            this.txtBoxTax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label6.Location = new System.Drawing.Point(228, 238);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 20);
            this.label6.TabIndex = 11;
            this.label6.Text = "testes";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 26);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "ID:";
            // 
            // txtBoxID
            // 
            this.txtBoxID.BackColor = System.Drawing.Color.White;
            this.txtBoxID.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBoxID.Location = new System.Drawing.Point(61, 23);
            this.txtBoxID.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtBoxID.Name = "txtBoxID";
            this.txtBoxID.Size = new System.Drawing.Size(111, 20);
            this.txtBoxID.TabIndex = 1;
            this.txtBoxID.Text = "XY03482342";
            // 
            // butComprar
            // 
            this.butComprar.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.butComprar.Location = new System.Drawing.Point(9, 19);
            this.butComprar.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.butComprar.Name = "butComprar";
            this.butComprar.Size = new System.Drawing.Size(72, 23);
            this.butComprar.TabIndex = 13;
            this.butComprar.Text = "Comprar";
            this.butComprar.UseVisualStyleBackColor = true;
            this.butComprar.Click += new System.EventHandler(this.butComprar_Click);
            // 
            // butCriar
            // 
            this.butCriar.Location = new System.Drawing.Point(9, 127);
            this.butCriar.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.butCriar.Name = "butCriar";
            this.butCriar.Size = new System.Drawing.Size(243, 25);
            this.butCriar.TabIndex = 4;
            this.butCriar.Text = "Criar Novo";
            this.butCriar.UseVisualStyleBackColor = true;
            this.butCriar.Click += new System.EventHandler(this.butCriar_Click);
            // 
            // butAltTax
            // 
            this.butAltTax.Location = new System.Drawing.Point(195, 160);
            this.butAltTax.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.butAltTax.Name = "butAltTax";
            this.butAltTax.Size = new System.Drawing.Size(57, 23);
            this.butAltTax.TabIndex = 7;
            this.butAltTax.Text = "Alterar";
            this.butAltTax.UseVisualStyleBackColor = true;
            this.butAltTax.Click += new System.EventHandler(this.butAltTax_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(160, 165);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "%";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(174, 79);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "€";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labelAsteriskMud);
            this.groupBox1.Controls.Add(this.nrUpDown1);
            this.groupBox1.Controls.Add(this.butNrcard);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.nrUpDown);
            this.groupBox1.Controls.Add(this.labelAsteriskTax);
            this.groupBox1.Controls.Add(this.txtBoxPnt);
            this.groupBox1.Controls.Add(this.labelAsteriskSal);
            this.groupBox1.Controls.Add(this.labelAsteriskTit);
            this.groupBox1.Controls.Add(this.butTransf);
            this.groupBox1.Controls.Add(this.butAltTax);
            this.groupBox1.Controls.Add(this.txtBoxTax);
            this.groupBox1.Controls.Add(this.labelAsteriskId);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtBoxTit);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.butAltTit);
            this.groupBox1.Controls.Add(this.txtBoxSal);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtBoxID);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.butCriar);
            this.groupBox1.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Black;
            this.groupBox1.Location = new System.Drawing.Point(11, 12);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox1.Size = new System.Drawing.Size(261, 223);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cartão Cliente";
            // 
            // labelAsteriskMud
            // 
            this.labelAsteriskMud.AutoSize = true;
            this.labelAsteriskMud.ForeColor = System.Drawing.Color.Red;
            this.labelAsteriskMud.Location = new System.Drawing.Point(172, 105);
            this.labelAsteriskMud.Name = "labelAsteriskMud";
            this.labelAsteriskMud.Size = new System.Drawing.Size(13, 13);
            this.labelAsteriskMud.TabIndex = 36;
            this.labelAsteriskMud.Text = "*";
            // 
            // nrUpDown1
            // 
            this.nrUpDown1.Location = new System.Drawing.Point(152, 189);
            this.nrUpDown1.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nrUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nrUpDown1.Name = "nrUpDown1";
            this.nrUpDown1.Size = new System.Drawing.Size(37, 20);
            this.nrUpDown1.TabIndex = 34;
            this.nrUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // butNrcard
            // 
            this.butNrcard.Location = new System.Drawing.Point(117, 100);
            this.butNrcard.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.butNrcard.Name = "butNrcard";
            this.butNrcard.Size = new System.Drawing.Size(56, 23);
            this.butNrcard.TabIndex = 30;
            this.butNrcard.Text = "Mudar";
            this.butNrcard.UseVisualStyleBackColor = true;
            this.butNrcard.Click += new System.EventHandler(this.butNrcard_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 105);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 29;
            this.label8.Text = "Cartão:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 191);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Pnts:";
            // 
            // nrUpDown
            // 
            this.nrUpDown.Location = new System.Drawing.Point(61, 102);
            this.nrUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nrUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nrUpDown.Name = "nrUpDown";
            this.nrUpDown.Size = new System.Drawing.Size(51, 20);
            this.nrUpDown.TabIndex = 28;
            this.nrUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // labelAsteriskTax
            // 
            this.labelAsteriskTax.AutoSize = true;
            this.labelAsteriskTax.ForeColor = System.Drawing.Color.Red;
            this.labelAsteriskTax.Location = new System.Drawing.Point(170, 165);
            this.labelAsteriskTax.Name = "labelAsteriskTax";
            this.labelAsteriskTax.Size = new System.Drawing.Size(13, 13);
            this.labelAsteriskTax.TabIndex = 27;
            this.labelAsteriskTax.Text = "*";
            // 
            // txtBoxPnt
            // 
            this.txtBoxPnt.BackColor = System.Drawing.Color.White;
            this.txtBoxPnt.Location = new System.Drawing.Point(46, 188);
            this.txtBoxPnt.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtBoxPnt.Name = "txtBoxPnt";
            this.txtBoxPnt.Size = new System.Drawing.Size(73, 20);
            this.txtBoxPnt.TabIndex = 32;
            this.txtBoxPnt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // labelAsteriskSal
            // 
            this.labelAsteriskSal.AutoSize = true;
            this.labelAsteriskSal.ForeColor = System.Drawing.Color.Red;
            this.labelAsteriskSal.Location = new System.Drawing.Point(183, 79);
            this.labelAsteriskSal.Name = "labelAsteriskSal";
            this.labelAsteriskSal.Size = new System.Drawing.Size(13, 13);
            this.labelAsteriskSal.TabIndex = 26;
            this.labelAsteriskSal.Text = "*";
            // 
            // labelAsteriskTit
            // 
            this.labelAsteriskTit.AutoSize = true;
            this.labelAsteriskTit.ForeColor = System.Drawing.Color.Red;
            this.labelAsteriskTit.Location = new System.Drawing.Point(172, 52);
            this.labelAsteriskTit.Name = "labelAsteriskTit";
            this.labelAsteriskTit.Size = new System.Drawing.Size(13, 13);
            this.labelAsteriskTit.TabIndex = 25;
            this.labelAsteriskTit.Text = "*";
            // 
            // butTransf
            // 
            this.butTransf.Location = new System.Drawing.Point(195, 187);
            this.butTransf.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.butTransf.Name = "butTransf";
            this.butTransf.Size = new System.Drawing.Size(57, 23);
            this.butTransf.TabIndex = 31;
            this.butTransf.Text = "Transf.";
            this.butTransf.UseVisualStyleBackColor = true;
            this.butTransf.Click += new System.EventHandler(this.butTransf_Click);
            // 
            // labelAsteriskId
            // 
            this.labelAsteriskId.AutoSize = true;
            this.labelAsteriskId.ForeColor = System.Drawing.Color.Red;
            this.labelAsteriskId.Location = new System.Drawing.Point(172, 26);
            this.labelAsteriskId.Name = "labelAsteriskId";
            this.labelAsteriskId.Size = new System.Drawing.Size(13, 13);
            this.labelAsteriskId.TabIndex = 24;
            this.labelAsteriskId.Text = "*";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(121, 192);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 35;
            this.label10.Text = "Crt:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 165);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(97, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "Taxa de prémio:";
            // 
            // butAltTit
            // 
            this.butAltTit.Location = new System.Drawing.Point(195, 47);
            this.butAltTit.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.butAltTit.Name = "butAltTit";
            this.butAltTit.Size = new System.Drawing.Size(57, 23);
            this.butAltTit.TabIndex = 6;
            this.butAltTit.Text = "Alterar";
            this.butAltTit.UseVisualStyleBackColor = true;
            this.butAltTit.Click += new System.EventHandler(this.butAltTit_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelAsteriskCmp);
            this.groupBox2.Controls.Add(this.comboBoxItems);
            this.groupBox2.Controls.Add(this.butComprar);
            this.groupBox2.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groupBox2.Location = new System.Drawing.Point(11, 263);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox2.Size = new System.Drawing.Size(261, 51);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Centro Comercial";
            // 
            // labelAsteriskCmp
            // 
            this.labelAsteriskCmp.AutoSize = true;
            this.labelAsteriskCmp.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Italic);
            this.labelAsteriskCmp.ForeColor = System.Drawing.Color.Red;
            this.labelAsteriskCmp.Location = new System.Drawing.Point(234, 25);
            this.labelAsteriskCmp.Name = "labelAsteriskCmp";
            this.labelAsteriskCmp.Size = new System.Drawing.Size(13, 13);
            this.labelAsteriskCmp.TabIndex = 32;
            this.labelAsteriskCmp.Text = "*";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.labelAsteriskLvn);
            this.groupBox3.Controls.Add(this.comboBoxPremios);
            this.groupBox3.Controls.Add(this.butLevantar);
            this.groupBox3.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groupBox3.Location = new System.Drawing.Point(11, 333);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox3.Size = new System.Drawing.Size(261, 51);
            this.groupBox3.TabIndex = 22;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Prémios";
            // 
            // labelAsteriskLvn
            // 
            this.labelAsteriskLvn.AutoSize = true;
            this.labelAsteriskLvn.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Italic);
            this.labelAsteriskLvn.ForeColor = System.Drawing.Color.Red;
            this.labelAsteriskLvn.Location = new System.Drawing.Point(234, 24);
            this.labelAsteriskLvn.Name = "labelAsteriskLvn";
            this.labelAsteriskLvn.Size = new System.Drawing.Size(13, 13);
            this.labelAsteriskLvn.TabIndex = 31;
            this.labelAsteriskLvn.Text = "*";
            // 
            // comboBoxPremios
            // 
            this.comboBoxPremios.BackColor = System.Drawing.Color.White;
            this.comboBoxPremios.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPremios.Font = new System.Drawing.Font("Consolas", 7.75F, System.Drawing.FontStyle.Italic);
            this.comboBoxPremios.FormattingEnabled = true;
            this.comboBoxPremios.Location = new System.Drawing.Point(85, 21);
            this.comboBoxPremios.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.comboBoxPremios.Name = "comboBoxPremios";
            this.comboBoxPremios.Size = new System.Drawing.Size(149, 20);
            this.comboBoxPremios.TabIndex = 0;
            // 
            // butLevantar
            // 
            this.butLevantar.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.butLevantar.Location = new System.Drawing.Point(9, 19);
            this.butLevantar.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.butLevantar.Name = "butLevantar";
            this.butLevantar.Size = new System.Drawing.Size(72, 23);
            this.butLevantar.TabIndex = 13;
            this.butLevantar.Text = "Levantar";
            this.butLevantar.UseVisualStyleBackColor = true;
            this.butLevantar.Click += new System.EventHandler(this.butLevantar_Click);
            // 
            // labelAsteriskInf
            // 
            this.labelAsteriskInf.AutoSize = true;
            this.labelAsteriskInf.Font = new System.Drawing.Font("Consolas", 7.25F, System.Drawing.FontStyle.Italic);
            this.labelAsteriskInf.ForeColor = System.Drawing.Color.Red;
            this.labelAsteriskInf.Location = new System.Drawing.Point(12, 237);
            this.labelAsteriskInf.Name = "labelAsteriskInf";
            this.labelAsteriskInf.Size = new System.Drawing.Size(10, 12);
            this.labelAsteriskInf.TabIndex = 28;
            this.labelAsteriskInf.Text = "*";
            // 
            // labelAsteriskInf2
            // 
            this.labelAsteriskInf2.AutoSize = true;
            this.labelAsteriskInf2.Font = new System.Drawing.Font("Consolas", 7.25F, System.Drawing.FontStyle.Italic);
            this.labelAsteriskInf2.ForeColor = System.Drawing.Color.Red;
            this.labelAsteriskInf2.Location = new System.Drawing.Point(12, 386);
            this.labelAsteriskInf2.Name = "labelAsteriskInf2";
            this.labelAsteriskInf2.Size = new System.Drawing.Size(10, 12);
            this.labelAsteriskInf2.TabIndex = 29;
            this.labelAsteriskInf2.Text = "*";
            // 
            // labelAsteriskInf1
            // 
            this.labelAsteriskInf1.AutoSize = true;
            this.labelAsteriskInf1.Font = new System.Drawing.Font("Consolas", 7.25F, System.Drawing.FontStyle.Italic);
            this.labelAsteriskInf1.ForeColor = System.Drawing.Color.Red;
            this.labelAsteriskInf1.Location = new System.Drawing.Point(12, 316);
            this.labelAsteriskInf1.Name = "labelAsteriskInf1";
            this.labelAsteriskInf1.Size = new System.Drawing.Size(10, 12);
            this.labelAsteriskInf1.TabIndex = 30;
            this.labelAsteriskInf1.Text = "*";
            // 
            // Pablo
            // 
            this.Pablo.Interval = 200;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(553, 405);
            this.Controls.Add(this.labelAsteriskInf1);
            this.Controls.Add(this.labelAsteriskInf2);
            this.Controls.Add(this.labelAsteriskInf);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.labelInfo);
            this.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nrUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nrUpDown)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxItems;
        private System.Windows.Forms.Label labelInfo;
        private System.Windows.Forms.TextBox txtBoxTit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtBoxSal;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBoxTax;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtBoxID;
        private System.Windows.Forms.Button butComprar;
        private System.Windows.Forms.Button butCriar;
        private System.Windows.Forms.Button butAltTax;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button butAltTit;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox comboBoxPremios;
        private System.Windows.Forms.Button butLevantar;
        private System.Windows.Forms.Label labelAsteriskTax;
        private System.Windows.Forms.Label labelAsteriskSal;
        private System.Windows.Forms.Label labelAsteriskTit;
        private System.Windows.Forms.Label labelAsteriskId;
        private System.Windows.Forms.Label labelAsteriskInf;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown nrUpDown;
        private System.Windows.Forms.Button butNrcard;
        private System.Windows.Forms.Label labelAsteriskCmp;
        private System.Windows.Forms.Label labelAsteriskLvn;
        private System.Windows.Forms.Label labelAsteriskInf2;
        private System.Windows.Forms.Label labelAsteriskInf1;
        private System.Windows.Forms.Button butTransf;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtBoxPnt;
        private System.Windows.Forms.NumericUpDown nrUpDown1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label labelAsteriskMud;
        private System.Windows.Forms.Timer Pablo;
    }
}

