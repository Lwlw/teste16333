﻿namespace Teste_16333
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class CartaoCliente
    {

        #region Atributos


        private string _id;

        private string _titular;

        private double _saldo;

        private int _pontos;

        private double _taxa;



        private int PontosBonus = 0;

        private List<string> itemComprPrem = new List<string>();
        private List<double> valorComprPrem = new List<double>();

        public int factor_ = 1;
        public List<int> factors_ = new List<int>();

        #endregion



        #region Propriedades

        public string Id { get; set; }

        public string Titular { get; set; }

        public double Saldo { get; set; }

        public int Pontos { get; set; }

        public double Taxa { get; set; }



        #endregion



        #region Construtores


        public CartaoCliente() : this("??", "??", 0.0) { }

        public CartaoCliente(string id, string nome, double saldo)
        {
            Id = id;
            Titular = nome;
            Saldo = saldo;
            Pontos = 0;
            Taxa = 10.0;
        }

        public CartaoCliente(CartaoCliente cc) : this(cc.Id, cc.Titular, cc.Saldo)
        {
            Id = cc.Id;
            Titular = cc.Titular;
            Saldo = cc.Saldo;
            Pontos = cc.Pontos;
            Taxa = cc.Taxa;
        }

        #endregion



        #region Métodos Gerais


        public override string ToString()
        {
            StringBuilder s = new StringBuilder();
            double totao1 = 0, totao2 = 0;

            int movimentos;
            int i2;

            //s.Append("------------ Dados do Cartão ------------\n\n");
            s.Append("ID: " + Id + "\n");
            s.Append("Titular: " + Titular + "\n");
            s.Append("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
            s.Append("Saldo: " + Saldo.ToString("0.00") + " Euros\n");
            s.Append("Pontos: " + Pontos + " Pontos\n");
            s.Append("Taxa de Prémio: " + Taxa.ToString("0.00") + "%\n");
            s.Append("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
            s.Append("---------- Últimas Transações -----------\n\n");

            if (itemComprPrem.Count == 0) { s.Append("Sem movimentações------------------------\n"); goto NoMovs; }

            //int mvm = 0;
            movimentos = 13;
            
            for (int i = 0; i < itemComprPrem.Count && i < movimentos; i++)
            {
                //factors_.Add(1);
                i2 = itemComprPrem.Count - movimentos + i;

                if (itemComprPrem.Count < movimentos ? !itemComprPrem[i].Contains("prém_") : !itemComprPrem[i2].Contains("prém_"))
                {
                    if (itemComprPrem.Count < movimentos)
                    {
                        //if (i + 1 < itemComprPrem.Count && itemComprPrem[i] == itemComprPrem[i + 1])
                        //{
                        //    continue;
                        //}

                        //else if (i != 0 && i + 1 == itemComprPrem.Count && itemComprPrem[i] == itemComprPrem[i - 1])
                        //    factors_[i] = Convert.ToInt32(OneFactor(i).Split('(')[1].Replace(
                        //    "Qtd.", String.Empty).Replace(")", String.Empty).Trim());

                        s.Append(itemComprPrem[i] + "".PadLeft((31 - itemComprPrem[i].Length), '-') +
                            valorComprPrem[i].ToString().PadLeft(9, '-') + /* Euros*/"$\n");
                    }

                    else
                    {

                        //if (i2 + 1 < itemComprPrem.Count && itemComprPrem[i2] == itemComprPrem[i2 + 1])
                        //    continue;

                        //else if (i2 != 0 && i2 + 1 == itemComprPrem.Count && itemComprPrem[i2] == itemComprPrem[i2 - 1])
                        //    factors_[i2] = Convert.ToInt32(OneFactor(i2).Split('(')[1].Replace(
                        //    "Qtd.", String.Empty).Replace(")", String.Empty).Trim());

                        s.Append(itemComprPrem[i2] + "".PadLeft((31 - itemComprPrem[i2].Length), '-') +
                            valorComprPrem[i2].ToString().PadLeft(9, '-') + /* Euros*/"$\n");
                    }
                }
                else
                {
                    if (itemComprPrem.Count < 13)
                    {
                        s.Append(itemComprPrem[i] + "".PadLeft((31 - itemComprPrem[i].Length), '-') +
                            valorComprPrem[i].ToString().PadLeft(7, '-') + /* Euros*/"Pts\n");
                    }
                    else
                    {
                        s.Append(itemComprPrem[itemComprPrem.Count - 13 + i] + "".PadLeft((31 -
                        itemComprPrem[itemComprPrem.Count - 13 + i].Length), '-') +
                            valorComprPrem[valorComprPrem.Count - 13 + i].ToString().PadLeft(7, '-') + /* Euros */"Pts\n");
                    }
                }

            }

            
            for (int j = 0; j < valorComprPrem.Count; j++)
            {
                if (!itemComprPrem[j].Contains("prém"))
                    totao1 += valorComprPrem[j];
                else
                    totao2 += valorComprPrem[j];
            }

            NoMovs:

            //s.Append(String.Format("-----------------------------------------\n", movimentos.ToString()));
            s.Append("\nTOTAL(despesas)" + "".PadLeft((26 - "Total(despesas)".Length), '-') +
                totao1.ToString("N2").PadLeft(9, '-') + " Euros\n");
            s.Append("TOTAL(prémios)" + "".PadLeft((26 - "Total(prémios)".Length), '-') +
                totao2.ToString("N2").PadLeft(8, '-') + " Pontos\n");
            //s.Append("-----------------------------------------\n");


            return s.ToString();

            //System.IO.File.WriteAllText(@"C:\Users\WriteText.txt", s.ToString());


        }

        #endregion



        #region Métodos


        public void AlterarTaxa(double taxa)  //mesmo ke ir pela propriedade... 
        {
            Taxa = taxa;
        }

        public void AlterarTitular(string titular)
        {
            Titular = titular;
        }

        public void AtribuirPontos(int pontos)
        {
            PontosBonus += pontos;
            Pontos += pontos;
            if (PontosBonus >= 50)
            {
                Pontos += (5 * (PontosBonus / 50));
                PontosBonus = (PontosBonus - PontosBonus / 50 * 50);
            }

        }

        public void LevantarPremio(int pontos, string premio)
        {
            string i = itemComprPrem[itemComprPrem.Count - 1];
            int fct = 1;

            if (i.Contains("prém"))
                fct = Convert.ToInt32(i.Split('.')[1].Replace(")", String.Empty).Trim());

            i = i.Substring(0, i.Length - (fct > 999 ? 10 : fct > 99 ? 9 : fct > 9 ? 8 : 7));

            if (i != premio)
            {
                itemComprPrem.Add(premio + "(Qtd.1)");
                valorComprPrem.Add(pontos);
            }
            else
            {
                fct++;
                itemComprPrem[itemComprPrem.Count - 1] = premio + "(Qtd." + fct + ")";
                valorComprPrem[itemComprPrem.Count - 1] = fct * pontos;
            }

            Pontos -= pontos;
        }

        public void EfetuarCompra(double valor, string artigo)
        {

            if (itemComprPrem.Count < 1)
            {
                itemComprPrem.Add(artigo + "(Qtd.1)");
                valorComprPrem.Add(valor);
            }
            else
            {
                string i = itemComprPrem[itemComprPrem.Count - 1];
                int fct = 1;

                if (!i.Contains("prém"))
                    fct = Convert.ToInt32(i.Split('.')[1].Replace(")", String.Empty).Trim());

                i = i.Substring(0, i.Length - (fct > 999 ? 10 : fct > 99 ? 9 : fct > 9 ? 8 : 7));

                if (i != artigo)
                {
                    itemComprPrem.Add(artigo + "(Qtd.1)");
                    valorComprPrem.Add(valor);
                }
                else
                {
                    fct++;
                    itemComprPrem[itemComprPrem.Count - 1] = artigo + "(Qtd." + fct + ")";
                    valorComprPrem[itemComprPrem.Count - 1] = fct * valor;
                }
            }

            Saldo = Saldo - valor;
            AtribuirPontos(Convert.ToInt32(Math.Floor(valor * Taxa / 100)));
        }

        public void TranferirPontos(int pontos, CartaoCliente cc)
        {
            if (pontos <= Pontos && cc != null)
            {
                Pontos -= pontos;
                cc.Pontos += pontos;
            } 
        }

        //agora, descomplicar e fazer melhor em 15 minutos... nah, nunca descomplikar. always waste time whenevr possibçe
        //string OneFactor(int index)
        //{
        //    int index2 = index;
        //    int count = 1;
        //    while (index2 - 1 >= 0)
        //        if (itemComprPrem[index] == itemComprPrem[index2 - 1])
        //        {
        //            count++;
        //            index2--;
        //        }
        //        else
        //            break;
        //    return itemComprPrem[index] + String.Format(" (Qtd.{0})", count);
        //}

        #endregion

    }
}
